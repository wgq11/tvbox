1. ◎ Github托管直播源打不开：地址前增加　https://mirror.ghproxy.com/ 或 https://github.moeyy.xyz/ 即可
2. ◎ m3u源转txt源：地址前增加　https://fanmingming.com/txt?url= 即可
3. ◎ txt转m3u：AnyConv　黑鸟博客


# IPTV

#### IPTV搜索引擎

1. http://tonkiang.us
2. https://iptv-org.github.io
3. https://www.foodieguide.com/iptvsearch/
4. https://streamtest.in

#### 精选项目

1. https://www.iptvindex.com
2. https://github.com/iptv-org/iptv
3. https://github.com/dimaston
4. https://github.com/Moexin/IPTV
5. https://github.com/joevess/IPTV
6. https://github.com/YanG-1989/m3u
7. https://github.com/Ftindy/IPTV-URL 
8. https://github.com/Cyril0563/lanjing_live
9. https://github.com/biancangming/wtv
10. https://github.com/whpsky/iptv
11. https://github.com/BigBigGrandG/IPTV-URL
12. https://github.com/zbefine/iptv
13. https://github.com/Kimentanm/aptv
14. https://github.com/YueChan/Live
15. https://github.com/fanmingming/live
16. https://github.com/Meroser/IPTV
17. https://github.com/qwerttvv/Beijing-IPTV
18. https://github.com/imDazui/Tvlist-awesome-m3u-m3u8

#### 他山之石

1. https://dimaston.github.io/live.m3u
2. https://iptv.b2og.com/
3. https://m3u.ibert.me/
4. https://live.freetv.top/huyayqk.m3u 虎牙一起看
5. https://live.freetv.top/douyuyqk.m3u 斗鱼一起看
6. https://live.freetv.top/yylunbo.m3u?url=https://lunbo.freetv.top YY轮播
7. https://www.goodiptv.club/bililive.m3u BiliBili直播
8. https://mirror.ghproxy.com/https://raw.githubusercontent.com/vamoschuck/TV/main/M3U 茶客
9. https://mirror.ghproxy.com/https://raw.githubusercontent.com/wuyun999/wuyun/main/zb/aptv.txt 乌云
10. https://mirror.ghproxy.com/https://raw.githubusercontent.com/hussobaba/AILE-Tv/main/TEBER_TV.m3u AILE-TV
11. https://mirror.ghproxy.com/https://raw.githubusercontent.com/goolguy007/radioer/main/TVradio TVradio
12. http://52bsj.vip:81/api/v3/file/get/79119/ZB.txt?sign=iChDXDC7WJRTp7yWAyVdbY3si5sJ5eFwEgaR35YQGSo%3D%3A0
13. https://zzzzz.tv/tv/tv.txt
14. https://zzzzz.tv/tv/live.txt
15. https://gitee.com/amygotv/tvsource/raw/master/TVLives
16. https://agit.ai/guot54/ygbh/raw/branch/master/zB/zB.txt
17. http://111.67.196.181/mtvzb.txt
18. https://sourl.cn/y4kFym
19. https://agit.ai/138001380000/MHQTV/raw/branch/master/TV/0903zh.txt
20. https://tvsee.github.io/diyp/tv.txt
21. https://iptv-org.github.io/iptv/index.nsfw.m3u
22. https://raw.githubusercontent.com/cai23511/yex/master/TVlist/20210808384.m3u
23. https://raw.githubusercontent.com/cai23511/yex/master/TVlist/20210808226.m3u
24. http://owen2000wy.github.io


#### 节目信息

1. https://epg.pw/
2. https://epg.pw/test_channels.m3u epg.pw 中国地区可观看
3. https://epg.pw/test_channels_banned_cn.m3u epg.pw 海外地区可观看
4. https://epg.pw/test_channels_unknown.m3u epg.pw 未添加EPG的大杂烩直播源
5. https://epg.pw/test_channel_page.html?lang=zh-hant epg.pw 直播源按分类列表
6. https://epg.112114.xyz/pp.xml
7. http://epg.51zmt.top:8000/e.xml
8. http://epg.aptvapp.com/xml
9. https://epg.pw/xmltv.html?lang=zh-hant


#### 随机轮换壁纸：
1. https://jianbian.chuqiuyu.workers.dev 自制极简渐变壁纸
2. http://www.kf666888.cn/api/tvbox/img
3. https://picsum.photos/1280/720/?blur=10
4. http://刚刚.live/图
5. http://饭.eu.org/深色壁纸/api.php,
6. https://www.dmoe.cc/random.php
7. https://api.btstu.cn/sjbz/zsy.php
8. https://api.btstu.cn/sjbz/?lx=dongman
9. http://api.btstu.cn/sjbz/?lx=meizi
10. http://api.btstu.cn/sjbz/?lx=suiji
11. https://pictures.catvod.eu.org/
12. https://bing.img.run/rand.php

#### Reference

1. http://adultiptv.net

#### 精选直播源

https://live.freetv.top/huyayqk.m3u    虎牙一起看

https://live.freetv.top/douyuyqk.m3u    斗鱼一起看

https://www.goodiptv.club/yylunbo.m3u?url=https://lunbo.freetv.top    YY轮播

https://www.goodiptv.club/bililive.m3u    BiliBili直播

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Ftindy/IPTV-URL/main/IPV6.m3u    精选IPV6源

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Ftindy/IPTV-URL/main/cqyx.m3u    CQYX源

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Ftindy/IPTV-URL/main/yqgd.m3u    YQGD源

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Ftindy/IPTV-URL/main/IPTV.m3u    4K/8K源

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Ftindy/IPTV-URL/main/msp.m3u    MSP源

1. BigBigGrandG

https://raw.githubusercontent.com/BigBigGrandG/IPTV-URL/release/Gather.m3u    BigBigGrandG源

2. APTV

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Kimentanm/aptv/master/m3u/iptv.m3u    APTV IPv6

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Kimentanm/aptv/master/m3u/aptv-playback.m3u    APTV 回放测试源

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Kimentanm/aptv/master/m3u/ya.m3u    APTV 虎牙

3. YanG

https://mirror.ghproxy.com/https://raw.githubusercontent.com/YanG-1989/m3u/main/Gather.m3u    YanG Gather

https://mirror.ghproxy.com/https://raw.githubusercontent.com/YanG-1989/m3u/main/yu.m3u    YanG 斗鱼

4. Meroser

https://mirror.ghproxy.com/https://raw.githubusercontent.com/Meroser/IPTV/main/IPTV.m3u    Meroser精选IPV6源

5. YueChan

https://mirror.ghproxy.com/https://raw.githubusercontent.com/YueChan/Live/main/IPTV.m3u    YueChan IPv6

https://mirror.ghproxy.com/https://raw.githubusercontent.com/YueChan/Live/main/Radio.m3u    YueChan Radio

6. whpsky

https://mirror.ghproxy.com/https://raw.githubusercontent.com/whpsky/iptv/main/IPTV-IPV6.m3u    whpsky-IPV6

https://mirror.ghproxy.com/https://raw.githubusercontent.com/whpsky/iptv/main/chinatv.m3u    whpsky-ChinaTVM3u

https://mirror.ghproxy.com/https://raw.githubusercontent.com/whpsky/iptv/main/chinatv.txt    whpsky-ChinaTVTxt

7. zbefine

https://mirror.ghproxy.com/https://raw.githubusercontent.com/zbefine/iptv/main/iptv.m3u    zbefine-m3u

https://mirror.ghproxy.com/https://raw.githubusercontent.com/zbefine/iptv/main/iptv.txt    zbefine-txt

8. 其他大神

https://mirror.ghproxy.com/https://raw.githubusercontent.com/vamoschuck/TV/main/M3U    茶客

https://mirror.ghproxy.com/https://raw.githubusercontent.com/wuyun999/wuyun/main/zb/aptv.txt    乌云

https://mirror.ghproxy.com/https://raw.githubusercontent.com/hussobaba/AILE-Tv/main/TEBER_TV.m3u    AILE-TV

https://mirror.ghproxy.com/https://raw.githubusercontent.com/goolguy007/radioer/main/TVradio    TVradio

https://epg.pw/test_channels.m3u    epg.pw 中国地区可观看

https://epg.pw/test_channels_banned_cn.m3u    epg.pw 海外地区可观看

https://epg.pw/test_channels_unknown.m3u    epg.pw 未添加EPG的大杂烩直播源

https://epg.pw/test_channel_page.html?lang=zh-hant    epg.pw 直播源按分类列表

9. EPG 节目单

https://epg.112114.xyz/pp.xml

http://epg.51zmt.top:8000/e.xml

http://epg.aptvapp.com/xml

https://epg.pw/xmltv.html?lang=zh-hant


#### 工具

1. 📆EPG接口地址：https://live.fanmingming.com/e.xml
2. 🏞️Bing每日图片：https://fanmingming.com/bing  或  https://bing.img.run/rand.php 
3. 🎞️m3u8下载工具：https://live.fanmingming.com/m3u8
4. 🆕TXT转M3U格式：https://live.fanmingming.com/txt2m3u
5. 📄在线M3U转TXT：Demo🔗 https://fanmingming.com/txt?url=https://live.fanmingming.com/tv/m3u/ipv6.m3u
6. 🌐M3U8 Web Player: Demo🔗 https://live.fanmingming.com/player/?vurl=https://0472.org/hls/cgtn.m3u8